#include "EXCLRunII/Pair.h"

double PairCandidate::acoplanarity() const{


  if(m_type == -1113 || m_type == 1311) return ( 1. - acos(cos(m_electron1->phi()-m_muon1->phi()))/TMath::Pi() );  
  
  if(m_type == 11 || m_type == 1111) return ( 1. - acos(cos(m_electron1->phi()-m_electron2->phi()))/TMath::Pi() );  
  
  if(m_type == 13 || m_type == 1313) return ( 1. - acos(cos(m_muon1->phi()-m_muon2->phi()))/TMath::Pi() );  
  
  return -1.;

}

double PairCandidate::delta_z0_leptons() const{

  //if(m_type == -1113 || m_type == 1311) return -1000.;  
  
  if(m_type == 11 || m_type == 1111){
  
    const xAOD::TrackParticle* tp1 = m_electron1->trackParticle();
    const xAOD::TrackParticle* tp2 = m_electron2->trackParticle(); 
    return ( (tp1->z0() + tp1->vz()) - (tp2->z0() + tp2->vz()) );  
  
  }
  
  if(m_type == 13 || m_type == 1313){
    
    const xAOD::TrackParticle *mutrk1 = m_muon1->primaryTrackParticle();
    const xAOD::TrackParticle *mutrk2 = m_muon2->primaryTrackParticle(); 
    return ( (mutrk1->z0() + mutrk1->vz()) - (mutrk2->z0() + mutrk2->vz()) );   
  
  }
 
  
  return -10.;

}

double PairCandidate::z0sintheta1() const{

  
  if(m_type == 11 || m_type == 1111){
  
    const xAOD::TrackParticle* tp1 = m_electron1->trackParticle();
    const xAOD::TrackParticle* tp2 = m_electron2->trackParticle(); 
    float z_from_leptons = 0.5*((tp1->z0() + tp1->vz()) + (tp2->z0() + tp2->vz()));
    return ( (tp1->z0() + tp1->vz() - z_from_leptons)*sin(tp1->theta()) );  
  
  }
  
  if(m_type == 13 || m_type == 1313){
    
    const xAOD::TrackParticle *mutrk1 = m_muon1->primaryTrackParticle();
    const xAOD::TrackParticle *mutrk2 = m_muon2->primaryTrackParticle(); 
    float z_from_leptons = 0.5*((mutrk1->z0() + mutrk1->vz()) + (mutrk2->z0() + mutrk2->vz()));
    return ( (mutrk1->z0() + mutrk1->vz() - z_from_leptons)*sin(mutrk1->theta()) );   
  
  }

  if(m_type == -1113 || m_type == 1311){
    
    const xAOD::TrackParticle *mutrk1 = m_muon1->primaryTrackParticle();
    const xAOD::TrackParticle* tp1 = m_electron1->trackParticle(); 
    float z_from_leptons = 0.5*((mutrk1->z0() + mutrk1->vz()) + (tp1->z0() + tp1->vz()));
    return ( (mutrk1->z0() + mutrk1->vz() - z_from_leptons)*sin(mutrk1->theta()) );   
  
  }
 
  
  return -10.;

}

double PairCandidate::z0sintheta2() const{


  
  if(m_type == 11 || m_type == 1111){
  
    const xAOD::TrackParticle* tp1 = m_electron2->trackParticle();
    const xAOD::TrackParticle* tp2 = m_electron1->trackParticle(); 
    float z_from_leptons = 0.5*((tp1->z0() + tp1->vz()) + (tp2->z0() + tp2->vz()));
    return ( (tp1->z0() + tp1->vz() - z_from_leptons)*sin(tp1->theta()) );  
  
  }
  
  if(m_type == 13 || m_type == 1313){
    
    const xAOD::TrackParticle *mutrk1 = m_muon2->primaryTrackParticle();
    const xAOD::TrackParticle *mutrk2 = m_muon1->primaryTrackParticle(); 
    float z_from_leptons = 0.5*((mutrk1->z0() + mutrk1->vz()) + (mutrk2->z0() + mutrk2->vz()));
    return ( (mutrk1->z0() + mutrk1->vz() - z_from_leptons)*sin(mutrk1->theta()) );   
  
  }

  if(m_type == -1113 || m_type == 1311){
    
    const xAOD::TrackParticle *mutrk1 = m_muon1->primaryTrackParticle();
    const xAOD::TrackParticle* tp1 = m_electron1->trackParticle(); 
    float z_from_leptons = 0.5*((mutrk1->z0() + mutrk1->vz()) + (tp1->z0() + tp1->vz()));
    return ( (tp1->z0() + tp1->vz() - z_from_leptons)*sin(tp1->theta()) );   
  
  } 
  
  return -10.;

}
