#ifndef HISTMANAGERMUON_H
#define HISTMANAGERMUON_H

#include <TH1.h>
#include <map>
#include "EXCLRunII/EXCLcandidate.h"
#include "EXCLRunII/ExclVeto.h"
#include "EXCLRunII/Pair.h"
#include "EXCLRunII/HistManager.h"

 
class HistManagerMuon { 

  public: 
   
  TString HM_name = "/Muon/";
  
  std::map<TString, TH1*> bookHistos(TString CurrentShift, TString Analysis);                                 //! books all histograms
  
  void fillHistos(const EXCLCandidate* E, float weight, std::map<TString, TH1*> hm, TString CurrentShift, TString Analysis); //! fills all histograms  
  
};

#endif
