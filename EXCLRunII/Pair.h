#ifndef PAIR_H
#define PAIR_H
  
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/VertexContainer.h"


 
class PairCandidate {

  public:
  
     PairCandidate(TLorentzVector v, int type, const xAOD::Electron* elec1, const xAOD::Electron* elec2) :
		m_fourMomentum(v), m_type(type), m_electron1(elec1), m_electron2(elec2) { }
     PairCandidate(TLorentzVector v, int type, const xAOD::Muon* muon1, const xAOD::Muon* muon2) :
		m_fourMomentum(v), m_type(type), m_muon1(muon1), m_muon2(muon2) { } 
     PairCandidate(TLorentzVector v, int type, const xAOD::Electron* electron1, const xAOD::Muon* muon1) :
		m_fourMomentum(v), m_type(type), m_electron1(electron1), m_muon1(muon1) { } 

     PairCandidate(TLorentzVector v, int type, const xAOD::Photon* g1, const xAOD::Photon* g2) :
		m_fourMomentum(v), m_type(type), m_photon1(g1), m_photon2(g2) { }


     PairCandidate(TLorentzVector v, int type, const xAOD::Electron* electron1, const xAOD::Photon* photon1) :
		m_fourMomentum(v), m_type(type), m_electron1(electron1), m_photon1(photon1) { } 

     PairCandidate(): m_type(0) { }
     ~PairCandidate() { }
		     
     const TLorentzVector fourMomentum() const { return m_fourMomentum; }
     int   type() const {return m_type; }
     const xAOD::Electron* electron1() const { return m_electron1; }
     const xAOD::Electron* electron2() const { return m_electron2; }
     const xAOD::Muon* muon1() const { return m_muon1; }
     const xAOD::Muon* muon2() const { return m_muon2; } 

     const xAOD::Photon* photon1() const { return m_photon1; }
     const xAOD::Photon* photon2() const { return m_photon2; }
     
     double acoplanarity() const; 
     double delta_z0_leptons() const; 
     double z0sintheta1() const;
     double z0sintheta2() const;    
  
  private:
     TLorentzVector m_fourMomentum;
     int m_type;
     const xAOD::Electron* m_electron1;
     const xAOD::Electron* m_electron2;
     const xAOD::Muon* m_muon1;
     const xAOD::Muon* m_muon2;       

     const xAOD::Photon* m_photon1;
     const xAOD::Photon* m_photon2;

};

typedef std::vector<PairCandidate*> PairCandidateContainer;

#endif
