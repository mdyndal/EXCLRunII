#ifndef EXCLEVENT_H
#define EXCLEVENT_H
 
#include "EXCLRunII/ExclVeto.h"
#include "EXCLRunII/Pair.h"



class EXCLEvent {

  public:
   
    EXCLCandidate( int type, const PairCandidate* Paircandidate, const ExclVetoCandidate* ExclVetocandidate ) :
		 m_type(type), m_Paircandidate(Paircandidate), m_ExclVetocandidate(ExclVetocandidate) { }
		 
    EXCLCandidate() { }	 
    ~EXCLCandidate() { }

    int   type() const {return m_type; }
    const PairCandidate* Paircandidate() const { return m_Paircandidate; }
    const ExclVetoCandidate* ExclVetocandidate() const { return m_ExclVetocandidate; }   
    
  private: 
  
    int m_type;
    const PairCandidate* m_Paircandidate;
    const ExclVetoCandidate* m_ExclVetocandidate;
      
};
  
#endif
