#ifndef HISTMANAGERELECTRON_H
#define HISTMANAGERELECTRON_H

#include <TH1.h>
#include <map>
#include "EXCLRunII/EXCLcandidate.h"
#include "EXCLRunII/ExclVeto.h"
#include "EXCLRunII/Pair.h"
 
 
class HistManagerElectron { 

  public: 
  
  TString HM_name = "/Electron/";
  
  std::map<TString, TH1*> bookHistos(TString CurrentShift, TString Analysis);                                 //! books all histograms
  
  void fillHistos(const EXCLCandidate* E, float weight, std::map<TString, TH1*> hm, TString CurrentShift, TString Analysis); //! books all histograms  
  
};

#endif
